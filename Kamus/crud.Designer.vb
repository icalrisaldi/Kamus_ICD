﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class crud
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtidpenyakit = New System.Windows.Forms.TextBox()
        Me.txtnamapenyakit = New System.Windows.Forms.TextBox()
        Me.txtidtindakan = New System.Windows.Forms.TextBox()
        Me.txtnamatindakan = New System.Windows.Forms.TextBox()
        Me.insert = New System.Windows.Forms.Button()
        Me.update = New System.Windows.Forms.Button()
        Me.deletepenyakit = New System.Windows.Forms.Button()
        Me.pencarian = New System.Windows.Forms.TextBox()
        Me.list = New System.Windows.Forms.ListView()
        Me.idpen = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.namapen = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.idtin = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.namatin = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.sort = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtdeskripsipenyakit = New System.Windows.Forms.RichTextBox()
        Me.txtdeskripsitindakan = New System.Windows.Forms.RichTextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ya = New System.Windows.Forms.RadioButton()
        Me.tidak = New System.Windows.Forms.RadioButton()
        Me.deletetindakan = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "ID Penyakit"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(79, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Nama Penyakit"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 143)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "ID Tindakan"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(13, 166)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(83, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Nama Tindakan"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(13, 262)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(84, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Dalam Ruangan"
        '
        'txtidpenyakit
        '
        Me.txtidpenyakit.Location = New System.Drawing.Point(115, 24)
        Me.txtidpenyakit.Name = "txtidpenyakit"
        Me.txtidpenyakit.Size = New System.Drawing.Size(100, 20)
        Me.txtidpenyakit.TabIndex = 5
        '
        'txtnamapenyakit
        '
        Me.txtnamapenyakit.Location = New System.Drawing.Point(115, 48)
        Me.txtnamapenyakit.Name = "txtnamapenyakit"
        Me.txtnamapenyakit.Size = New System.Drawing.Size(100, 20)
        Me.txtnamapenyakit.TabIndex = 6
        '
        'txtidtindakan
        '
        Me.txtidtindakan.Location = New System.Drawing.Point(115, 143)
        Me.txtidtindakan.Name = "txtidtindakan"
        Me.txtidtindakan.Size = New System.Drawing.Size(100, 20)
        Me.txtidtindakan.TabIndex = 7
        '
        'txtnamatindakan
        '
        Me.txtnamatindakan.Location = New System.Drawing.Point(115, 166)
        Me.txtnamatindakan.Name = "txtnamatindakan"
        Me.txtnamatindakan.Size = New System.Drawing.Size(100, 20)
        Me.txtnamatindakan.TabIndex = 8
        '
        'insert
        '
        Me.insert.Location = New System.Drawing.Point(294, 24)
        Me.insert.Name = "insert"
        Me.insert.Size = New System.Drawing.Size(75, 23)
        Me.insert.TabIndex = 10
        Me.insert.Text = "Tambah"
        Me.insert.UseVisualStyleBackColor = True
        '
        'update
        '
        Me.update.Location = New System.Drawing.Point(294, 53)
        Me.update.Name = "update"
        Me.update.Size = New System.Drawing.Size(75, 23)
        Me.update.TabIndex = 11
        Me.update.Text = "Update"
        Me.update.UseVisualStyleBackColor = True
        '
        'deletepenyakit
        '
        Me.deletepenyakit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.deletepenyakit.BackColor = System.Drawing.Color.Red
        Me.deletepenyakit.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.deletepenyakit.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.deletepenyakit.Location = New System.Drawing.Point(221, 24)
        Me.deletepenyakit.Name = "deletepenyakit"
        Me.deletepenyakit.Size = New System.Drawing.Size(54, 23)
        Me.deletepenyakit.TabIndex = 12
        Me.deletepenyakit.Text = "Hapus"
        Me.deletepenyakit.UseVisualStyleBackColor = False
        '
        'pencarian
        '
        Me.pencarian.ForeColor = System.Drawing.SystemColors.ScrollBar
        Me.pencarian.Location = New System.Drawing.Point(12, 292)
        Me.pencarian.Name = "pencarian"
        Me.pencarian.Size = New System.Drawing.Size(84, 20)
        Me.pencarian.TabIndex = 13
        Me.pencarian.Text = "Cari..."
        '
        'list
        '
        Me.list.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.idpen, Me.namapen, Me.idtin, Me.namatin})
        Me.list.Location = New System.Drawing.Point(12, 318)
        Me.list.Name = "list"
        Me.list.Size = New System.Drawing.Size(349, 153)
        Me.list.TabIndex = 14
        Me.list.UseCompatibleStateImageBehavior = False
        Me.list.View = System.Windows.Forms.View.Details
        '
        'idpen
        '
        Me.idpen.Text = "ID"
        '
        'namapen
        '
        Me.namapen.Text = "Nama Penyakit"
        Me.namapen.Width = 103
        '
        'idtin
        '
        Me.idtin.Text = "ID"
        '
        'namatin
        '
        Me.namatin.Text = "Nama Tindakan"
        Me.namatin.Width = 121
        '
        'sort
        '
        Me.sort.ForeColor = System.Drawing.SystemColors.ScrollBar
        Me.sort.FormattingEnabled = True
        Me.sort.Items.AddRange(New Object() {"Penyakit", "Tindakan"})
        Me.sort.Location = New System.Drawing.Point(114, 291)
        Me.sort.Name = "sort"
        Me.sort.Size = New System.Drawing.Size(74, 21)
        Me.sort.TabIndex = 15
        Me.sort.Text = "Search By"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 75)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(50, 13)
        Me.Label6.TabIndex = 16
        Me.Label6.Text = "Deskripsi"
        '
        'txtdeskripsipenyakit
        '
        Me.txtdeskripsipenyakit.Location = New System.Drawing.Point(115, 72)
        Me.txtdeskripsipenyakit.Name = "txtdeskripsipenyakit"
        Me.txtdeskripsipenyakit.Size = New System.Drawing.Size(100, 63)
        Me.txtdeskripsipenyakit.TabIndex = 17
        Me.txtdeskripsipenyakit.Text = ""
        '
        'txtdeskripsitindakan
        '
        Me.txtdeskripsitindakan.Location = New System.Drawing.Point(114, 192)
        Me.txtdeskripsitindakan.Name = "txtdeskripsitindakan"
        Me.txtdeskripsitindakan.Size = New System.Drawing.Size(101, 63)
        Me.txtdeskripsitindakan.TabIndex = 19
        Me.txtdeskripsitindakan.Text = ""
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 192)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(50, 13)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "Deskripsi"
        '
        'ya
        '
        Me.ya.AutoSize = True
        Me.ya.Location = New System.Drawing.Point(114, 262)
        Me.ya.Name = "ya"
        Me.ya.Size = New System.Drawing.Size(38, 17)
        Me.ya.TabIndex = 20
        Me.ya.TabStop = True
        Me.ya.Text = "Ya"
        Me.ya.UseVisualStyleBackColor = True
        '
        'tidak
        '
        Me.tidak.AutoSize = True
        Me.tidak.Location = New System.Drawing.Point(158, 262)
        Me.tidak.Name = "tidak"
        Me.tidak.Size = New System.Drawing.Size(52, 17)
        Me.tidak.TabIndex = 21
        Me.tidak.TabStop = True
        Me.tidak.Text = "Tidak"
        Me.tidak.UseVisualStyleBackColor = True
        '
        'deletetindakan
        '
        Me.deletetindakan.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.deletetindakan.BackColor = System.Drawing.Color.Red
        Me.deletetindakan.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.deletetindakan.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.deletetindakan.Location = New System.Drawing.Point(221, 143)
        Me.deletetindakan.Name = "deletetindakan"
        Me.deletetindakan.Size = New System.Drawing.Size(54, 23)
        Me.deletetindakan.TabIndex = 22
        Me.deletetindakan.Text = "Hapus"
        Me.deletetindakan.UseVisualStyleBackColor = False
        '
        'crud
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(381, 483)
        Me.Controls.Add(Me.deletetindakan)
        Me.Controls.Add(Me.tidak)
        Me.Controls.Add(Me.ya)
        Me.Controls.Add(Me.txtdeskripsitindakan)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtdeskripsipenyakit)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.sort)
        Me.Controls.Add(Me.list)
        Me.Controls.Add(Me.pencarian)
        Me.Controls.Add(Me.deletepenyakit)
        Me.Controls.Add(Me.update)
        Me.Controls.Add(Me.insert)
        Me.Controls.Add(Me.txtnamatindakan)
        Me.Controls.Add(Me.txtidtindakan)
        Me.Controls.Add(Me.txtnamapenyakit)
        Me.Controls.Add(Me.txtidpenyakit)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "crud"
        Me.Text = "Crud"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtidpenyakit As TextBox
    Friend WithEvents txtnamapenyakit As TextBox
    Friend WithEvents txtidtindakan As TextBox
    Friend WithEvents txtnamatindakan As TextBox
    Friend WithEvents insert As Button
    Friend WithEvents update As Button
    Friend WithEvents deletepenyakit As Button
    Friend WithEvents pencarian As TextBox
    Friend WithEvents list As ListView
    Friend WithEvents idpen As ColumnHeader
    Friend WithEvents namapen As ColumnHeader
    Friend WithEvents idtin As ColumnHeader
    Friend WithEvents namatin As ColumnHeader
    Friend WithEvents sort As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtdeskripsipenyakit As RichTextBox
    Friend WithEvents txtdeskripsitindakan As RichTextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents ya As RadioButton
    Friend WithEvents tidak As RadioButton
    Friend WithEvents deletetindakan As Button
End Class
