﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class master
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pencarian = New System.Windows.Forms.TextBox()
        Me.cari = New System.Windows.Forms.Button()
        Me.penyakit = New System.Windows.Forms.ListView()
        Me.idpenyakit = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.namapenyakit = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.tindakan = New System.Windows.Forms.ListView()
        Me.idtindakan = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.namatindakan = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.idpen = New System.Windows.Forms.Label()
        Me.namapen = New System.Windows.Forms.Label()
        Me.despen = New System.Windows.Forms.Label()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.idtin = New System.Windows.Forms.Label()
        Me.namatin = New System.Windows.Forms.Label()
        Me.destin = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.OleDbConnection1 = New System.Data.OleDb.OleDbConnection()
        Me.tambah = New System.Windows.Forms.Button()
        Me.informasi = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'pencarian
        '
        Me.pencarian.ForeColor = System.Drawing.SystemColors.ScrollBar
        Me.pencarian.Location = New System.Drawing.Point(12, 46)
        Me.pencarian.Name = "pencarian"
        Me.pencarian.Size = New System.Drawing.Size(156, 20)
        Me.pencarian.TabIndex = 0
        Me.pencarian.Text = "Pencarian..."
        '
        'cari
        '
        Me.cari.Location = New System.Drawing.Point(175, 45)
        Me.cari.Name = "cari"
        Me.cari.Size = New System.Drawing.Size(49, 23)
        Me.cari.TabIndex = 2
        Me.cari.Text = "Cari"
        Me.cari.UseVisualStyleBackColor = True
        '
        'penyakit
        '
        Me.penyakit.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.idpenyakit, Me.namapenyakit})
        Me.penyakit.Location = New System.Drawing.Point(13, 73)
        Me.penyakit.Name = "penyakit"
        Me.penyakit.Size = New System.Drawing.Size(155, 220)
        Me.penyakit.TabIndex = 3
        Me.penyakit.UseCompatibleStateImageBehavior = False
        Me.penyakit.View = System.Windows.Forms.View.Details
        '
        'idpenyakit
        '
        Me.idpenyakit.Text = "ID"
        Me.idpenyakit.Width = 45
        '
        'namapenyakit
        '
        Me.namapenyakit.Text = "Nama Penyakit"
        Me.namapenyakit.Width = 106
        '
        'tindakan
        '
        Me.tindakan.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.idtindakan, Me.namatindakan})
        Me.tindakan.Location = New System.Drawing.Point(13, 299)
        Me.tindakan.Name = "tindakan"
        Me.tindakan.Size = New System.Drawing.Size(155, 220)
        Me.tindakan.TabIndex = 4
        Me.tindakan.UseCompatibleStateImageBehavior = False
        Me.tindakan.View = System.Windows.Forms.View.Details
        '
        'idtindakan
        '
        Me.idtindakan.Text = "ID"
        Me.idtindakan.Width = 42
        '
        'namatindakan
        '
        Me.namatindakan.Text = "Nama Tindakan"
        Me.namatindakan.Width = 109
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.TableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.83333!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 79.16666!))
        Me.TableLayoutPanel1.Controls.Add(Me.idpen, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.namapen, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.despen, 1, 1)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(175, 74)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.50228!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 89.49772!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(240, 219)
        Me.TableLayoutPanel1.TabIndex = 5
        '
        'idpen
        '
        Me.idpen.AutoSize = True
        Me.idpen.Location = New System.Drawing.Point(4, 1)
        Me.idpen.Name = "idpen"
        Me.idpen.Size = New System.Drawing.Size(0, 13)
        Me.idpen.TabIndex = 0
        '
        'namapen
        '
        Me.namapen.AutoSize = True
        Me.namapen.Location = New System.Drawing.Point(54, 1)
        Me.namapen.Name = "namapen"
        Me.namapen.Size = New System.Drawing.Size(0, 13)
        Me.namapen.TabIndex = 1
        '
        'despen
        '
        Me.despen.AutoSize = True
        Me.despen.Location = New System.Drawing.Point(54, 24)
        Me.despen.Name = "despen"
        Me.despen.Size = New System.Drawing.Size(0, 13)
        Me.despen.TabIndex = 2
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.TableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.83333!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 79.16666!))
        Me.TableLayoutPanel2.Controls.Add(Me.idtin, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.namatin, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.destin, 1, 1)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(175, 300)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.50228!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 89.49772!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(240, 219)
        Me.TableLayoutPanel2.TabIndex = 6
        '
        'idtin
        '
        Me.idtin.AutoSize = True
        Me.idtin.Location = New System.Drawing.Point(4, 1)
        Me.idtin.Name = "idtin"
        Me.idtin.Size = New System.Drawing.Size(0, 13)
        Me.idtin.TabIndex = 0
        '
        'namatin
        '
        Me.namatin.AutoSize = True
        Me.namatin.Location = New System.Drawing.Point(54, 1)
        Me.namatin.Name = "namatin"
        Me.namatin.Size = New System.Drawing.Size(0, 13)
        Me.namatin.TabIndex = 1
        '
        'destin
        '
        Me.destin.AutoSize = True
        Me.destin.Location = New System.Drawing.Point(54, 24)
        Me.destin.Name = "destin"
        Me.destin.Size = New System.Drawing.Size(0, 13)
        Me.destin.TabIndex = 2
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(7, 9)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(153, 31)
        Me.Label9.TabIndex = 9
        Me.Label9.Text = "Kamus ICD"
        '
        'tambah
        '
        Me.tambah.Location = New System.Drawing.Point(363, 46)
        Me.tambah.Name = "tambah"
        Me.tambah.Size = New System.Drawing.Size(23, 23)
        Me.tambah.TabIndex = 10
        Me.tambah.Text = "+"
        Me.tambah.UseVisualStyleBackColor = True
        '
        'informasi
        '
        Me.informasi.Location = New System.Drawing.Point(392, 46)
        Me.informasi.Name = "informasi"
        Me.informasi.Size = New System.Drawing.Size(23, 23)
        Me.informasi.TabIndex = 11
        Me.informasi.Text = "i"
        Me.informasi.UseVisualStyleBackColor = True
        '
        'master
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(427, 526)
        Me.Controls.Add(Me.informasi)
        Me.Controls.Add(Me.tambah)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.tindakan)
        Me.Controls.Add(Me.penyakit)
        Me.Controls.Add(Me.cari)
        Me.Controls.Add(Me.pencarian)
        Me.Name = "master"
        Me.Text = "ICD 9 And 10"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pencarian As TextBox
    Friend WithEvents cari As Button
    Friend WithEvents penyakit As ListView
    Friend WithEvents idpenyakit As ColumnHeader
    Friend WithEvents namapenyakit As ColumnHeader
    Friend WithEvents tindakan As ListView
    Friend WithEvents idtindakan As ColumnHeader
    Friend WithEvents namatindakan As ColumnHeader
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents idpen As Label
    Friend WithEvents namapen As Label
    Friend WithEvents despen As Label
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents idtin As Label
    Friend WithEvents namatin As Label
    Friend WithEvents destin As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents OleDbConnection1 As OleDb.OleDbConnection
    Friend WithEvents tambah As Button
    Friend WithEvents informasi As Button
End Class
