﻿Imports System.Data.OleDb
Public Class crud
    Dim cn As New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Kamus.accdb")
    Dim cmd As OleDbCommand
    Dim cmd2 As New OleDbCommand
    Dim da As OleDbDataReader
    Dim dr As OleDbDataReader
    Dim cd As OleDbDataReader
    Dim ubah As String
    Dim filtercari As String
    Dim cekked As String

    Sub bersih()
        txtidpenyakit.Clear()
        txtnamapenyakit.Clear()
        txtdeskripsipenyakit.Clear()
        txtidtindakan.Clear()
        txtnamatindakan.Clear()
        txtdeskripsitindakan.Clear()
        ya.Checked = False
        tidak.Checked = False
        txtidpenyakit.Focus()
        txtidpenyakit.Enabled = True
        txtidtindakan.Enabled = True
        deletepenyakit.Visible = False
        deletetindakan.Visible = False
    End Sub
    Private Sub tambahdata()
        Dim sqlpen As String
        Dim sqltin As String
        Dim sqlptin As String
        Dim cek As String
        cn.Open()

        'cek penyakit
        cek = "Select * from penyakit where id_penyakit = '" & txtidpenyakit.Text & "' "
        cmd = New OleDbCommand(cek, cn)
        cd = cmd.ExecuteReader
        cd.Read()
        If cd.HasRows Then
        Else
            sqlpen = "INSERT INTO penyakit Values('" & txtidpenyakit.Text & "','" & txtnamapenyakit.Text & "','" & txtdeskripsipenyakit.Text & "');"
            cmd = New OleDbCommand(sqlpen, cn)
            cmd.ExecuteNonQuery()
        End If

        'cek tindakan
        cek = "Select * from tindakan where id_tindakan = '" & txtidtindakan.Text & "' "
        cmd = New OleDbCommand(cek, cn)
        cd = cmd.ExecuteReader
        cd.Read()
        If cd.HasRows Then
        Else
            sqltin = "INSERT INTO tindakan Values('" & txtidtindakan.Text & "','" & txtnamatindakan.Text & "','" & txtdeskripsitindakan.Text & "');"
            cmd = New OleDbCommand(sqltin, cn)
            cmd.ExecuteNonQuery()
        End If

        If ya.Checked = True Then
            sqlptin = "INSERT INTO p_tindakan Values('" & txtidpenyakit.Text & "','" & txtidtindakan.Text & "','0');"
        ElseIf tidak.Checked = True Then
            sqlptin = "INSERT INTO p_tindakan Values('" & txtidpenyakit.Text & "','" & txtidtindakan.Text & "','1');"
        End If


        cmd = New OleDbCommand(sqlptin, cn)
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub
    Private Sub hapusdata(tabel As String, kondisi As String)
        Dim sql As String
        If tabel = "penyakit" Then
            sql = "Delete * From penyakit WHERE ((id_penyakit)='" & kondisi & "');"
        ElseIf tabel = "tindakan" Then
            sql = "Delete * From tindakan WHERE ((id_tindakan)='" & kondisi & "');"
        End If
        cmd = New OleDbCommand(sql, cn)

        'execute
        cn.Open()
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub
    Private Sub updatedata(tabel As String, value As String, kolom As String, kondisi As String)
        Dim sql As String
        If tabel = "penyakit" Then
            sql = "UPDATE " & tabel & " SET " & kolom & " = '" & value & "' WHERE ((id_penyakit)='" & kondisi & "');"
        ElseIf tabel = "tindakan" Then
            sql = "UPDATE " & tabel & " SET " & kolom & " = '" & value & "' WHERE ((id_tindakan)='" & kondisi & "');"
        End If
        cmd = New OleDbCommand(sql, cn)

        'execute
        cn.Open()
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Private Sub updatedataint(tabel As String, value As Integer, kolom As String, kondisi As String)
        Dim sql As String = "UPDATE " & tabel & " SET ket = '" & value & "' WHERE ((" & kolom & ")='" & kondisi & "');"
        cmd = New OleDbCommand(sql, cn)

        'execute
        cn.Open()
        cmd.ExecuteNonQuery()
    End Sub
    Private Sub caridata()
        cn.Open()
        Dim kueri As New OleDbCommand
        ubah = pencarian.Text
        If (filtercari = "Penyakit") Then
            With kueri
                .Connection = cn
                .CommandText = "SELECT penyakit.id_penyakit, penyakit.nama_penyakit, tindakan.id_tindakan, tindakan.nama_tindakan 
                            FROM tindakan INNER JOIN (penyakit INNER JOIN p_tindakan ON penyakit.[id_penyakit] = p_tindakan.[id_penyakit]) 
                            ON tindakan.[id_tindakan] = p_tindakan.[id_tindakan] where 
                             (((penyakit.ID_penyakit) like '*" & ubah & "*')) OR 
                             (((penyakit.nama_penyakit) like '*" & ubah & "*'));"
            End With
        End If

        If (filtercari = "Tindakan") Then
            With kueri
                .Connection = cn
                .CommandText = "SELECT penyakit.id_penyakit, penyakit.nama_penyakit, tindakan.id_tindakan, tindakan.nama_tindakan 
                            FROM tindakan INNER JOIN (penyakit INNER JOIN p_tindakan ON penyakit.[id_penyakit] = p_tindakan.[id_penyakit]) 
                            ON tindakan.[id_tindakan] = p_tindakan.[id_tindakan] where 
                             (((tindakan.ID_penyakit) like '*" & ubah & "*')) OR 
                             (((tindakan.nama_penyakit) like '*" & ubah & "*'));"
            End With
        End If

        dr = kueri.ExecuteReader

        While dr.Read
            With list
                .Items.Add(dr.Item(0))
                With .Items(.Items.Count - 1).SubItems
                    .Add(dr(1))
                    .Add(dr(2))
                    .Add(dr(3))
                End With
            End With
        End While
        cn.Close()
    End Sub
    Private Sub cekdata(tabel As String, value As String)
        Dim cek As String
        If tabel = "penyakit" Then
            cek = "Select * from penyakit where id_penyakit = '" & value & "' "
        ElseIf tabel = "tindakan" Then
            cek = "Select * from tindakan where id_tindakan = '" & value & "' "
        End If

        cn.Open()
        cmd = New OleDbCommand(cek, cn)
        cd = cmd.ExecuteReader
        cd.Read()
        If cd.HasRows Then
            If tabel = "penyakit" Then
                txtidpenyakit.Text = cd.Item("id_penyakit")
                txtnamapenyakit.Text = cd.Item("nama_penyakit")
                txtdeskripsipenyakit.Text = cd.Item("deskripsi")
            ElseIf tabel = "tindakan" Then
                txtidtindakan.Text = cd.Item("id_tindakan")
                txtnamatindakan.Text = cd.Item("nama_tindakan")
                txtdeskripsitindakan.Text = cd.Item("deskripsi")
            End If
        End If

        cn.Close()
    End Sub
    Private Sub showdata()
        cn.Close()
        cn.Open()
        With cmd2
            .Connection = cn
            .CommandText = "SELECT penyakit.id_penyakit, penyakit.nama_penyakit, tindakan.id_tindakan, tindakan.nama_tindakan, penyakit.deskripsi,
                            tindakan.deskripsi, p_tindakan.ket 
                            FROM tindakan INNER JOIN (penyakit INNER JOIN p_tindakan ON penyakit.[id_penyakit] = p_tindakan.[id_penyakit]) 
                            ON tindakan.[id_tindakan] = p_tindakan.[id_tindakan] ;"
        End With
        da = cmd2.ExecuteReader
        While da.Read
            With list
                .Items.Add(da.Item(0))
                With .Items(.Items.Count - 1).SubItems
                    .Add(da(1))
                    .Add(da(2))
                    .Add(da(3))
                    .Add(da(4))
                    .Add(da(5))
                    .Add(da(6))
                End With
            End With
        End While
        cn.Close()
    End Sub
    Private Sub pencarian_MouseClick(sender As Object, e As MouseEventArgs) Handles pencarian.MouseClick
        pencarian.Clear()
        pencarian.ForeColor = Color.Black
    End Sub

    Private Sub crud_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        showdata()
        deletepenyakit.Visible = False
        deletetindakan.Visible = False
    End Sub

    Private Sub pencarian_TextChanged(sender As Object, e As EventArgs) Handles pencarian.TextChanged
        ubah = pencarian.Text
    End Sub

    Private Sub pencarian_KeyPress(sender As Object, e As KeyPressEventArgs) Handles pencarian.KeyPress
        If e.KeyChar = Chr(13) Then
            caridata()
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles sort.SelectedIndexChanged
        filtercari = sort.SelectedItem
        sort.ForeColor = Color.Black
        sort.Text = sort.SelectedItem
    End Sub

    Private Sub list_Click(sender As Object, e As EventArgs) Handles list.Click
        txtidpenyakit.Text = list.SelectedItems.Item(0).SubItems(0).Text
        txtidpenyakit.Enabled = False
        txtnamapenyakit.Text = list.SelectedItems.Item(0).SubItems(1).Text
        txtdeskripsipenyakit.Text = list.SelectedItems.Item(0).SubItems(4).Text
        txtidtindakan.Text = list.SelectedItems.Item(0).SubItems(2).Text
        txtidtindakan.Enabled = False
        txtnamatindakan.Text = list.SelectedItems.Item(0).SubItems(3).Text
        txtdeskripsitindakan.Text = list.SelectedItems.Item(0).SubItems(5).Text
        If ((list.SelectedItems.Item(0).SubItems(6).Text) = 0) Then
            ya.Checked = True
            tidak.Checked = False
        ElseIf ((list.SelectedItems.Item(0).SubItems(6).Text) = 1) Then
            ya.Checked = False
            tidak.Checked = True
        ElseIf ((list.SelectedItems.Item(0).SubItems(6).Text) = 2) Then
            ya.Checked = False
            tidak.Checked = False
        End If
        deletepenyakit.Visible = True
        deletetindakan.Visible = True
    End Sub

    Private Sub update_Click(sender As Object, e As EventArgs) Handles update.Click
        'Nama Penyakit
        updatedata("penyakit", txtnamapenyakit.Text, "nama_penyakit", txtidpenyakit.Text)
        'Deskripsi Penyakit
        updatedata("penyakit", txtdeskripsipenyakit.Text, "deskripsi", txtidpenyakit.Text)
        'Nama tindakan
        updatedata("tindakan", txtnamatindakan.Text, "nama_tindakan", txtidtindakan.Text)
        'Deskripsi tindakan
        updatedata("tindakan", txtdeskripsitindakan.Text, "deskripsi", txtidtindakan.Text)
        'Keterangan
        If ya.Checked = True Then
            updatedataint("p_tindakan", 0, "id_penyakit", txtidpenyakit.Text)
        ElseIf tidak.Checked = True Then
            updatedataint("p_tindakan", 1, "id_penyakit", txtidpenyakit.Text)
        ElseIf ya.Checked = False Or tidak.Checked = False Then
            updatedataint("p_tindakan", 2, "id_penyakit", txtidpenyakit.Text)
        End If
        bersih()
        list.Items.Clear()
        showdata()
    End Sub

    Private Sub insert_Click(sender As Object, e As EventArgs) Handles insert.Click
        tambahdata()
        bersih()
        list.Items.Clear()
        showdata()
    End Sub

    Private Sub deletetindakan_Click(sender As Object, e As EventArgs) Handles deletetindakan.Click
        Dim konfirmasi As MsgBoxResult
        konfirmasi = MsgBox("Yakin akan dihapus?", MsgBoxStyle.YesNo, "Peringatan")
        If konfirmasi = MsgBoxResult.Yes Then
            hapusdata("tindakan", txtidtindakan.Text)
            bersih()
            list.Items.Clear()
            showdata()
        Else
            txtnamapenyakit.Focus()
        End If
    End Sub

    Private Sub deletepenyakit_Click(sender As Object, e As EventArgs) Handles deletepenyakit.Click
        Dim konfirmasi As MsgBoxResult
        konfirmasi = MsgBox("Yakin akan dihapus?", MsgBoxStyle.YesNo, "Peringatan")
        If konfirmasi = MsgBoxResult.Yes Then
            hapusdata("penyakit", txtidpenyakit.Text)
            bersih()
            list.Items.Clear()
            showdata()
        Else
            txtnamapenyakit.Focus()
        End If
    End Sub

    Private Sub txtidpenyakit_Leave(sender As Object, e As EventArgs) Handles txtidpenyakit.Leave
        cekdata("penyakit", txtidpenyakit.Text)
    End Sub

    Private Sub txtidtindakan_Leave(sender As Object, e As EventArgs) Handles txtidtindakan.Leave
        cekdata("tindakan", txtidtindakan.Text)
    End Sub
End Class